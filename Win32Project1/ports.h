#pragma once

namespace neos {
	namespace ports {

		void write_byte(int port, int value);
		void write_short(int port, int value);
		void write_int(int port, int value);

		char read_byte(int port);
		short read_short(int port);
		int read_int(int port);

	}
}