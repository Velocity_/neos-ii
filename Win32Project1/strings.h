#pragma once

namespace neos {
	namespace util {
		int strlen(char *str);
		int strlen_s(char *str, int max);
		bool strcat(char *dest, char *src);
	}
}