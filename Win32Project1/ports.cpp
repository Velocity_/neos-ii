#include <ports.h>

void neos::ports::write_byte(int port, int value) {
	char v = (char)value;
	short p = (short)port;

	_asm {
		mov al, v;
		mov dx, p;
		out dx, al;
	}
}

void neos::ports::write_short(int port, int value) {
	short v = (short)value;
	short p = (short)port;

	_asm {
		mov ax, v;
		mov dx, p;
		out dx, ax;
	}
}

void neos::ports::write_int(int port, int value) {
	short p = (short)port;

	_asm {
		mov eax, value;
		mov dx, p;
		out dx, eax;
	}
}

char neos::ports::read_byte(int port) {
	short p = (short)port;
	char result = 0;

	_asm {
		mov dx, p;
		in al, dx;
		mov [result], al;
	}

	return result;
}

short neos::ports::read_short(int port) {
	short p = (short)port;
	short result = 0;

	_asm {
		mov dx, p;
		in ax, dx;
		mov[result], ax;
	}

	return result;
}

int neos::ports::read_int(int port) {
	short p = (short)port;
	int result = 0;

	_asm {
		mov dx, p;
		in eax, dx;
		mov[result], eax;
	}

	return result;
}