#include "vga.h"


neos::display::vga::vga() {
}


neos::display::vga::~vga() {
}

void neos::display::vga::clear_characters() {
	char *mem = (char*) VGA_BASE_ADDRESS;
	for (int i = 0; i < VGA_DISPLAY_SIZE; i++){
		*mem++ = ' '; /* Empty character (space) */
		*mem++ = 0x0F; /* White on black */
	}
}

void neos::display::vga::print_string(char *message) {
	/* Copy message ptr */
	char *local = message;

	/* Do the logic! */
	//TODO check for bounds
	while (*local != 0) {
		/* Test for line feed character */
		if (*local == '\n') {
			char *next_line = next_line_addr(vgaptr+2);
			if (next_line != nullptr) {
				vgaptr = next_line;
				local++;
				continue; /* Skip this character. */
			}
		}

		*vgaptr++ = *local; /* Character */
		*vgaptr++ = 0x0F; /* Color (white text on black bg) */
		local++;
	}

	/* Update cursor */
	update_cursor();
}

void neos::display::vga::scroll_display() {
	/* Move all the bytes a line up! */
	for (char *ptr = (char *) (VGA_BASE_ADDRESS + VGA_BYTES_PER_LINE); ptr < (char *) (VGA_BASE_ADDRESS + VGA_TOTAL_BYTES - VGA_BYTES_PER_LINE); ptr++) {
		*(ptr - VGA_BYTES_PER_LINE) = *ptr;
	}

	/* Clear the 'new' bytes */
	for (char *ptr = (char *)(VGA_BASE_ADDRESS + VGA_TOTAL_BYTES - VGA_BYTES_PER_LINE); ptr < (char *)(VGA_BASE_ADDRESS + VGA_TOTAL_BYTES); ptr++) {
		*(ptr - VGA_BYTES_PER_LINE) = *ptr;
	}

	/* Update the cursor * pointers properly */
	vgaptr -= VGA_DISPLAY_WIDTH * 2;
	update_cursor();
}

void neos::display::vga::set_hardware_cursor_position(char row, char col) {
	set_hardware_cursor_position((row * VGA_DISPLAY_WIDTH) + col);
}

void neos::display::vga::set_hardware_cursor_position(int idx) {
	ports::write_byte(0x3D4, 0x0F);
	ports::write_byte(0x3D5, idx & 0xFF);
	ports::write_byte(0x3D4, 0x0E);
	ports::write_byte(0x3D5, idx >> 8);
}

void neos::display::vga::update_cursor() {
	set_hardware_cursor_position((int)(vgaptr - VGA_BASE_ADDRESS) / 2);
}

void neos::display::vga::set_caret(int row, int col) {
	vgaptr = (char *) VGA_BASE_ADDRESS + (row * VGA_BYTES_PER_LINE) + (col * 2);
	update_cursor();
}

void neos::display::vga::print_number(long num, int base) {
	char temp[32];
	int tptr = 0;

	while (num != 0) {
		int toprint = num % base;

		if (toprint < 10)
			temp[tptr++] = static_cast<char>('0' + toprint);
		else
			temp[tptr++] = static_cast<char>(0x37 + toprint);

		num /= base;
	}

	for (int i = tptr - 1; i >= 0; i--) {
		*vgaptr++ = temp[i]; /* Character */
		*vgaptr++ = 0x0F; /* Color (white text on black bg) */
	}		

	update_cursor();
}

char *neos::display::vga::next_line_addr(char *from) {
	/* Iterate until we reach a point where the address minus base is dividable by the number of bytes in a line. */
	while ((int)(from - VGA_BASE_ADDRESS) % (VGA_DISPLAY_WIDTH * 2) != 0) {
		if ((int)from > (VGA_BASE_ADDRESS + VGA_DISPLAY_SIZE * 2)) /* Check if we're not going outside VGA memory */
			return nullptr;
		from++;
	}

	return from;
}

void neos::display::vga::kprintf(char *format, ...) {
	va_list ap;
	va_start(ap, format);

	char *end = neos::util::strlen(format) + format;

	while (format < end) {
		if (*format == '%' && format+1 < end) {
			format++;

			if (*format == 'd') {
				print_number(va_arg(ap, int), 10);
			}
			else if (*format == 'x') {
				print_number(va_arg(ap, int), 16);
			}
		} else {
			if (*format == '\n') {
				char *next_line = next_line_addr(vgaptr + 2);
				if (next_line != nullptr) {
					vgaptr = next_line;
				}
			}
			else {
				*vgaptr++ = *format;
				*vgaptr++ = 0x0F;
			}
		}

		format++;
	}

	va_end(ap);
	update_cursor();
}