#include <paging.h>

page_table_t *neos::paging::directory;

page_table_t *neos::paging::get_page_directory() {
	return directory;
}

page_t *neos::paging::get_page_table(int index) {
	return (page_t *) directory[index];
}

page_t *neos::paging::get_page_address(int index) {
	return (page_t *) (directory[index] & PAGE_ADDRESS_BITS);
}

int neos::paging::get_page_flags(int index) {
	return directory[index] & PAGE_FLAGS_BITS;
}

void neos::paging::setup_page_directory() {
	/* Scan for a page-aligned area */
	char *ptr = allocated_page_area;
	while ((int)ptr % PAGE_SIZE != 0)
		ptr++;

	/* Save it! */
	directory = (page_table_t *) ptr;

	/* Fill with empty tables */
	for (int i = 0; i < TABLES_PER_DIRECTORY; i++) {
		directory[i] = 0x00000002;
	}
}

void neos::paging::put_table(int index, page_table_t *table, bool writeable, bool supervisor, bool write_through, bool cache_disabled) {
	int flags = PAGE_FLAG_PRESENT;
	if (writeable)
		flags |= PAGE_FLAG_WRITEABLE;
	if (supervisor)
		flags |= PAGE_FLAG_SUPERVISOR;
	if (write_through)
		flags |= PAGE_FLAG_WRITE_THROUGH;
	if (cache_disabled)
		flags |= PAGE_FLAG_CACHE_DISABLED;

	directory[index] = ((int) table) | flags;
}

void neos::paging::map_page(uint32_t physical, uint32_t virtual_, bool writeable, bool supervisor) {
	int page = virtual_ / PAGE_SIZE;
	int dir = page / PAGE_SIZE;

	/* Check if the table exists */
	if (directory[dir] & PAGE_FLAG_PRESENT) {
		page_t *table = get_page_address(dir);

		/* Put our page in the table */
		int flags = PAGE_FLAG_PRESENT | (writeable ? PAGE_FLAG_WRITEABLE : 0) | (supervisor ? PAGE_FLAG_SUPERVISOR : 0);
		table[page] = (physical & PAGE_ADDRESS_BITS) | flags;
	} else {
		//TODO
	}
}