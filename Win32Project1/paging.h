#pragma once

#include <stdint.h>
#include <vga.h>

static char allocated_page_area[4096 * 3];

typedef uint32_t page_table_t;
typedef uint32_t page_t;

#define PAGE_FLAG_PRESENT 0x1
#define PAGE_FLAG_WRITEABLE 0x2
#define PAGE_FLAG_SUPERVISOR 0x4
#define PAGE_FLAG_WRITE_THROUGH 0x8
#define PAGE_FLAG_CACHE_DISABLED 0x10
#define PAGE_ADDRESS_BITS 0xFFFFF000
#define PAGE_FLAGS_BITS 0xFFF
#define PAGE_SIZE 0x1000
#define TABLES_PER_DIRECTORY 1024
#define PAGES_PER_TABLE 1024

namespace neos{
	class paging {

	private:
		static page_table_t *directory;

	public:
		static void set_wp_bit(bool enabled);
		static page_table_t *get_page_directory();
		static page_t *get_page_table(int index);
		static page_t *get_page_address(int index);
		static int get_page_flags(int index);
		static void setup_page_directory();
		static void put_table(int index, page_table_t *table, bool writeable, bool supervisor, bool write_through, bool cache_disabled);
		static void map_page(uint32_t physical, uint32_t virtual_, bool writeable, bool supervisor);

	};
}