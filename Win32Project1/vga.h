#pragma once

#include <strings.h>
#include <ports.h>
#include <stdarg.h>

#define VGA_BASE_ADDRESS (0xB8000)
#define VGA_DISPLAY_WIDTH (80)
#define VGA_DISPLAY_HEIGHT (25)
#define VGA_DISPLAY_SIZE (80 * 25)
#define VGA_TOTAL_BYTES (VGA_DISPLAY_SIZE * 2)
#define VGA_BYTES_PER_LINE (2 * VGA_DISPLAY_WIDTH)

namespace neos {
	namespace display {

		static char *vgaptr = (char *) VGA_BASE_ADDRESS;

		class vga {
		public:
			vga();
			~vga();
			static void clear_characters();
			static void print_string(char *message);
			static void print_number(long value, int base);
			static void scroll_display();
			static void set_caret(int row, int col);

			// RETURNS NULLPTR IF THERE IS NO NEXT LINE
			static char *next_line_addr(char *from);
			static void kprintf(char *format, ...);

		private:
			static void set_hardware_cursor_position(char row, char col);
			static void set_hardware_cursor_position(int index);
			static void update_cursor();
		};

	}
}
