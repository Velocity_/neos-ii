#pragma once
class x86 {
public:
	x86();
	~x86();

	static inline void x86::halt() {
		_asm hlt;
	}

	static inline void x86::sti() {
		_asm sti;
	}

	static inline void x86::cli() {
		_asm cli;
	}

	static int get_cr0();
	static int get_cr2();
	static int get_cr3();
	static void set_cr0(int value);
	static void set_cr2(int value);
	static void set_cr3(int value);

};

