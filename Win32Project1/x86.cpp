#include "x86.h"

x86::x86(){
}


x86::~x86(){
}

int x86::get_cr0() {
	int value = 0;

	_asm {
		mov eax, cr0;
		mov [value], eax;
	}

	return value;
}

int x86::get_cr2() {
	int value = 0;

	_asm {
		mov eax, cr2;
		mov[value], eax;
	}

	return value;
}

int x86::get_cr3() {
	int value = 0;

	_asm {
		mov eax, cr3;
		mov[value], eax;
	}

	return value;
}

void x86::set_cr0(int v) {
	_asm {
		mov eax, v;
		mov cr0, eax;
	}
}

void x86::set_cr2(int v) {
	_asm {
		mov eax, v;
		mov cr2, eax;
	}
}

void x86::set_cr3(int v) {
	_asm {
		mov eax, v;
		mov cr3, eax;
	}
}
