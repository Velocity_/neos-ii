#include "strings.h"

int neos::util::strlen(char *str) {
	char *copy = str;
	int length = 0;
	while (*copy != 0) {
		length++;
		copy++;
	}
	return length;
}

int neos::util::strlen_s(char *str, int max) {
	max += (int)str;
	char *copy = str;
	int length = 0;
	while (*copy != 0 && (int)copy != max) {
		length++;
		copy++;
	}
	return length;
}

bool neos::util::strcat(char *dest, char *src) {
	if (dest == nullptr || src == nullptr)
		return false;

	int destlen = strlen(dest);
	int srclen = strlen(src);

	char *ptr;
	for (ptr = dest + destlen; ptr < dest + destlen + srclen; ptr++) {
		*ptr = src[ptr - dest - destlen];
	}

	*ptr = '\0'; /* Null-terminator */
	return true;
}