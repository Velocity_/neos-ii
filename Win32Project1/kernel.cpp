#include <multiboot.h>
#include <vga.h>
#include <x86.h>
#include <ports.h>
#include <malloc.h>
#include <paging.h>

#define		ALIGN			0x400
#define		LOADBASE		0x100000
#define		HEADER_ADDRESS	LOADBASE+ALIGN

using namespace neos;
using namespace display;

namespace neos {

	void kernel_Startup(multiboot_header* header);
	void kernel_main(unsigned long magic, unsigned long addr);

#pragma code_seg(".a$0")
	/* Multiboot header */
	__declspec(allocate(".a$0"))
		multiboot_header _MultibootHeader = {
		MULTIBOOT_HEADER_MAGIC,
		MULTIBOOT_HEADER_FLAGS,
		CHECKSUM,
		HEADER_ADDRESS,
		LOADBASE,
		0, //load end address
		0, //bss end address
		(unsigned long)kernel_Startup,
		0,
		800,
		600,
		8
	};
#pragma comment(linker, "/merge:.text=.a")


	/* Kernel main entry point */
	void kernel_Startup(multiboot_header* header) {
		unsigned long magic;
		unsigned long mbiptr;

		/* Copy parameters from registers into local variables */
		__asm {
			mov magic, eax; // Move EAX into 'magic'
			mov mbiptr, ebx; // Move EBX into 'mbiptr' (ref to multiboot struct)
		}

		/* Start operating system */
		if (magic == 0x2BADB002) {
			kernel_main(magic, mbiptr);
		}

		x86::halt();
	}


	void _declspec(naked) interrupt_handler() {
		_asm pushad;

		_asm {
			popad;
			iretd;
		}
	}


	typedef struct idt_location {
		short size_in_bytes_minus_one;
		int address;
	};


	void setup_idt() {
		int location = 0;

		_asm {
			mov ecx, location;
			lidt ecx;
		}
	}


	void kernel_main(unsigned long magic, unsigned long addr) {
		vga::clear_characters();

		vga::kprintf("Hello and welcome to NeOS.\n");
		vga::kprintf("Hoe pointer: %d (%x)\n", allocated_page_area, allocated_page_area);

		paging::setup_page_directory();

		/* If we were able to set up the table memory we can now put our table there */
		page_table_t *page_directory = paging::get_page_directory();
		page_t *first_page = (page_t *) (page_directory + PAGE_SIZE);

		if (page_directory != nullptr) {
			/* Enable first page */
			paging::put_table(0, first_page, true, false, false, false);

			/* Put tables */
			for (int i = 0; i < PAGES_PER_TABLE; i++) {
				paging::map_page(i * PAGE_SIZE, i * PAGE_SIZE, true, false);
			}

			/* Map vga to 0x0 */
			paging::map_page(VGA_BASE_ADDRESS, 0, true, false);
		}

		x86::set_cr3((uint32_t)page_directory);
		x86::set_cr0(x86::get_cr0() | (1 << 31) | (1 << 16));

		vga::kprintf("Test?\n");
	}

}